# Quick start

## To install

gwcosmo requires Python >= 3.6

The easiest way to install it is with `conda` and `pip`.

See the [online documentation](https://ldas-jobs.ligo-wa.caltech.edu/~ignacio.magana/_build/html/installation.html) for the preferred installation instructions.
