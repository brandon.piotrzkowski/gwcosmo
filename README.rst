gwcosmo
=======

A package to estimate cosmological parameters using gravitational-wave observations.

-  `Installation instructions <https://ldas-jobs.ligo-wa.caltech.edu/~ignacio.magana/_build/html/installation.html>`__
-  `Contributing <https://git.ligo.org/cbc-cosmo/gwcosmo/blob/master/CONTRIBUTING.md>`__
-  `Documentation <https://ldas-jobs.ligo-wa.caltech.edu/~ignacio.magana/_build/html/index.html>`__
-  `Issue tracker <https://git.ligo.org/cbc-cosmo/gwcosmo/issues>`__
